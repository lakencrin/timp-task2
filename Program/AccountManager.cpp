#include "AccountManager.h"

#include <QStandardPaths>
#include <QStringList>
#include <QDir>
#include <QFileInfo>
#include <QUuid>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValueRef>
#include <QJsonObject>

#include <iostream>

AccountManager::AccountManager()
{
	QFile file{ AccountManager::managerFileName() };
	if (!file.open(QFile::ExistingOnly | QFile::ReadOnly))
		throw std::exception{ "Unexpected error" };

	QJsonDocument json = QJsonDocument::fromJson(file.readAll());
	for (auto& arrayObject : json.array())
	{
		QJsonObject object = arrayObject.toObject();
		QString const name = object.value("name").toString();
		QString const surname = object.value("surname").toString();
		QString const secondName = object.value("secondName").toString();
		QUuid const uuid = object.value("uuid").toString();
		uint64_t const timeRemaining = object.value("timeRemaining").toString().toULongLong();

		AccountInfo account{ { name, surname, secondName }, uuid, timeRemaining };
		_accounts.push_back(std::move(account));
	}
}

AccountManager::~AccountManager()
{
	QJsonArray array;
	for (auto& account : _accounts)
	{
		QJsonObject object;
		object.insert("name", account.fullName.name);
		object.insert("surname", account.fullName.surname);
		object.insert("secondName", account.fullName.secondName);
		object.insert("uuid", account.uuid.toString());
		object.insert("timeRemaining", QString::number(account.timeRemaining));
		array.append(object);
	}
	QJsonDocument document;
	document.setArray(array);

	QFile file{ AccountManager::managerFileName() };
	if (!file.open(QFile::ExistingOnly | QFile::WriteOnly))
		throw std::exception{ "Unexpected error" };

	file.write(document.toJson());
	file.close();
}

QString AccountManager::managerFileName()
{
	QString const appDataLocation = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).first();
	QDir appDataDir{ appDataLocation };
	if (!appDataDir.exists())
		appDataDir.mkpath(appDataDir.absolutePath());

	QFileInfo mngFileInfo{ appDataDir.filePath("accman.dat") };
	QString const mngFilePath = mngFileInfo.absoluteFilePath();
	if (!mngFileInfo.exists())
	{
		QFile file{ mngFilePath };
		file.open(QFile::OpenModeFlag::NewOnly);
		file.write("[]");
		file.close();
	}

	return mngFilePath;
}

bool AccountManager::check(const FullName& fullName) const
{
	return _accounts.end() != std::find_if(_accounts.begin(), _accounts.end(), [&fullName](const AccountInfo& info) -> bool { return info.fullName == fullName; });
}

void AccountManager::registerAccount(const FullName& fullName)
{
	if (check(fullName))
		throw std::invalid_argument{ "This user is already registered" };

	_accounts.push_back({ fullName, QUuid::createUuid(), 30000 });
}

SessionPointer AccountManager::startSession(const FullName& fullName)
{
	AccountInfo account = accountInfo(fullName);

	Session* session = new Session{ account };
	QObject::connect(session, &Session::destroyed, [this, session]() { sessionDestroyed(session); });

	return SessionPointer{ session };
}

void AccountManager::sessionDestroyed(Session* session)
{
	AccountInfo account = session->accountInfo();
	accountInfo(account.fullName).timeRemaining = account.timeRemaining;
}

AccountInfo AccountManager::accountInfo(const FullName& fullName) const
{
	auto point = std::find_if(_accounts.begin(), _accounts.end(), [&fullName](const AccountInfo& info) -> bool { return info.fullName == fullName; });
	if (point == _accounts.end())
		throw std::invalid_argument{ "No user with given name" };

	return *point;
}

AccountInfo& AccountManager::accountInfo(const FullName& fullName)
{
	auto point = std::find_if(_accounts.begin(), _accounts.end(), [&fullName](const AccountInfo& info) -> bool { return info.fullName == fullName; });
	if (point == _accounts.end())
		throw std::invalid_argument{ "No user with given name" };

	return *point;
}