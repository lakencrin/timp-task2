#pragma once

#include "AccountInfo.h"

#include <QObject>
#include <QTimer>

class Session : public QObject
{
	Q_OBJECT

	AccountInfo _account;

	QTimer _timer;

public:
	Session(AccountInfo account)
		: QObject{ nullptr }
		, _account{ account }
		, _timer{}
	{
		_timer.setInterval(100);
		QObject::connect(&_timer, &QTimer::timeout, this, &Session::onTimerShot);
	}

	AccountInfo accountInfo() const
	{
		return _account;
	}

	void start()
	{
		_timer.start();
	}

	uint64_t timeRemaining() const
	{
		return _account.timeRemaining;
	}

signals:
	void updated();
	void sessionExpired();

private slots:
	void onTimerShot()
	{
		if (_account.timeRemaining == 0)
			return;

		if (_account.timeRemaining <= 100)
		{
			_account.timeRemaining = 0;
			emit sessionExpired();
		}
		else
		{
			_account.timeRemaining -= 100;
			emit updated();
		}
	}
};

using SessionPointer = QSharedPointer<Session>;