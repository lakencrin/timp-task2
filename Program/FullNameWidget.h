#pragma once
#include "AccountInfo.h"

#include <QDialog>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QFormLayout>
#include <QLineEdit>


class FullNameWidget : public QDialog
{
	Q_OBJECT

	QLineEdit* _nameLineEdit;
	QLineEdit* _surnameLineEdit;
	QLineEdit* _secondNameLineEdit;

	QPushButton* _acceptButton;
	QPushButton* _rejectButton;

public:
	FullNameWidget(QWidget* parent = nullptr)
		: QDialog{ parent }
	{
		QFormLayout* layout = new QFormLayout;
		setLayout(layout);

		_nameLineEdit = new QLineEdit{ this };
		QObject::connect(_nameLineEdit, &QLineEdit::textChanged, this, &FullNameWidget::onLineEditChanged);
		_nameLineEdit->setPlaceholderText("Name");
		layout->addRow(_nameLineEdit);

		_surnameLineEdit = new QLineEdit{ this };
		QObject::connect(_surnameLineEdit, &QLineEdit::textChanged, this, &FullNameWidget::onLineEditChanged);
		_surnameLineEdit->setPlaceholderText("Surname");
		layout->addRow(_surnameLineEdit);

		_secondNameLineEdit = new QLineEdit{ this };
		QObject::connect(_secondNameLineEdit, &QLineEdit::textChanged, this, &FullNameWidget::onLineEditChanged);
		_secondNameLineEdit->setPlaceholderText("Name");
		layout->addRow(_secondNameLineEdit);

		_acceptButton = new QPushButton{ "OK", this };
		_acceptButton->setDisabled(true);
		QObject::connect(_acceptButton, &QPushButton::clicked, this, &QDialog::accept);

		_rejectButton = new QPushButton{ "Cancel", this };
		QObject::connect(_rejectButton, &QPushButton::clicked, this, &QDialog::reject);

		QHBoxLayout* buttonLayout = new QHBoxLayout;
		buttonLayout->setAlignment(Qt::AlignRight);
		buttonLayout->addWidget(_acceptButton);
		buttonLayout->addWidget(_rejectButton);
		layout->addRow(buttonLayout);
	}

	FullName fullName() const
	{
		return FullName{ _nameLineEdit->text(), _surnameLineEdit->text(), _secondNameLineEdit->text() };
	}
	
private slots:
	void onLineEditChanged()
	{
		_acceptButton->setDisabled(_nameLineEdit->text().isEmpty() || _surnameLineEdit->text().isEmpty() || _secondNameLineEdit->text().isEmpty());
	}
};