#include "AccountManager.h"
#include "FullNameWidget.h"
#include "MainWindow.h"

#include <QApplication>
#include <QMessageBox>

#include <iostream>

int main(int argc, char* argv[])
{
	QApplication app{ argc, argv };

	AccountManager mng;

	MainWindow mainWindow;
	FullNameWidget* fullNameWidget = new FullNameWidget{ &mainWindow };
	QDialog::DialogCode code = static_cast<QDialog::DialogCode>(fullNameWidget->exec());
	if (code == QDialog::DialogCode::Rejected)
		return 1;

	if (!mng.check(fullNameWidget->fullName()))
	{
		QMessageBox::information(&mainWindow, "Information", "New user is registered");
		mng.registerAccount(fullNameWidget->fullName());
	}

	SessionPointer session = mng.startSession(fullNameWidget->fullName());
	mainWindow.show(session);
	session->start();


	return app.exec();
}