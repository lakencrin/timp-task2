#pragma once
#include "Session.h"

#include <QMainWindow>


class MainWindow : public QWidget
{
	Q_OBJECT

	class QLabel* _fullNameLabel;
	class QLabel* _remainingTimeLabel;
	class QPushButton* _closeButton;

	SessionPointer _session;

public:
	MainWindow();
	~MainWindow() = default;

	void show(SessionPointer session);

private slots:
	void onUpdated();
	void onSessionExpired();

private:
	void setTimeRemaining(uint64_t);
};

