#pragma once

#include "AccountInfo.h"
#include "Session.h"

#include <vector>

class AccountManager
{
public:
	AccountManager();
	~AccountManager();

	bool check(const FullName& fullName) const;
	void registerAccount(const FullName& fullName);

	SessionPointer startSession(const FullName& fullName);

private:
	std::vector<AccountInfo> _accounts;

	AccountInfo& accountInfo(const FullName& fullName);
	AccountInfo accountInfo(const FullName& fullName) const;
	void sessionDestroyed(Session*);


	static QString managerFileName();
};

