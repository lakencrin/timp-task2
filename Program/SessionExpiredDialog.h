#pragma once
#include <QDialog>
#include <QDesktopServices>
#include <QVBoxLayout>
#include <QLabel>
#include <QUrl>
#include <QApplication>
#include <QPushButton>

class SessionExpiredDialog : public QDialog
{
public:
	SessionExpiredDialog(QWidget* parent = nullptr)
		: QDialog{ parent }
	{
		setWindowFlag(Qt::WindowCloseButtonHint, false);
		QVBoxLayout* layout = new QVBoxLayout{ this };
		layout->addWidget(new QLabel{ "Program evaluation time for current user has came to an end.\nBuy a full version if you want to continue.", this });

		QHBoxLayout* buttonLayout = new QHBoxLayout{ this };
		QPushButton* goToSiteButton = new QPushButton{ this };
		buttonLayout->addWidget(goToSiteButton);
		QObject::connect(goToSiteButton, &QPushButton::clicked, []() { QDesktopServices::openUrl(QUrl{ "https://gitlab.com/lakencrin/timp-task2" }); });
		QPushButton* closeButton = new QPushButton{ this };
		buttonLayout->addWidget(closeButton);
		QObject::connect(closeButton, &QPushButton::clicked, QApplication::instance(), &QApplication::exit);
		layout->addLayout(buttonLayout);
	}
};

