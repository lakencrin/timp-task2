#include "MainWindow.h"
#include "SessionExpiredDialog.h"

#include <QMessageBox>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QBoxLayout>

MainWindow::MainWindow()
	: QWidget{ nullptr }
	, _fullNameLabel{ new QLabel{ this } }
	, _remainingTimeLabel{ new QLabel{ this } }
{
	QVBoxLayout* layout = new QVBoxLayout;
	setLayout(layout);

	layout->addWidget(_fullNameLabel);
	layout->addWidget(_remainingTimeLabel);

	QHBoxLayout* buttonLayout = new QHBoxLayout;
	_closeButton = new QPushButton{ "Close", this };
	QObject::connect(_closeButton, &QPushButton::clicked, this, &MainWindow::close);
	buttonLayout->addWidget(_closeButton);

	layout->addLayout(buttonLayout);
}

void MainWindow::show(SessionPointer session)
{
	_session = session;
	AccountInfo accountInfo = session->accountInfo();
	FullName& fullName = accountInfo.fullName;
	_fullNameLabel->setText(QString{ "User name: %1 %2 %3" }.arg(fullName.surname).arg(fullName.name).arg(fullName.secondName));
	setTimeRemaining(accountInfo.timeRemaining);

	QObject::connect(session.get(), &Session::updated, this, &MainWindow::onUpdated);
	QObject::connect(session.get(), &Session::sessionExpired, this, &MainWindow::onSessionExpired);

	QWidget::show();
}

void MainWindow::onUpdated() 
{
	setTimeRemaining(_session->timeRemaining());
}
void MainWindow::onSessionExpired() 
{
	SessionExpiredDialog dialog;
	dialog.exec();
}

void MainWindow::setTimeRemaining(uint64_t timeRemaining)
{
	_remainingTimeLabel->setText(QString{ "Time remaining: %1.%2 sec" }.arg(timeRemaining / 1000).arg(timeRemaining / 100 % 10));
}