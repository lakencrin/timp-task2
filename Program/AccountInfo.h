#pragma once

#include <QString>
#include <QUuid>

#include <cstdlib>

struct FullName
{
	QString name;
	QString surname;
	QString secondName;

	bool operator==(const FullName& other) const
	{
		return name == other.name && surname == other.surname && secondName == other.secondName;
	}
};

struct AccountInfo
{
	FullName fullName;

	QUuid uuid;
	uint64_t timeRemaining;
};