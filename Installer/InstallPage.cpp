#include "InstallPage.h"
#include "Utils.h"
#include "PathPage.h"

#include <QMessageBox>
#include <QVBoxLayout>
#include <QLabel>

InstallPage::InstallPage()
	: QWizardPage{}
	, _completed{ false }
{
	QVBoxLayout* layout = new QVBoxLayout;
	setLayout(layout);
	setFinalPage(true);
}

void InstallPage::initializePage()
{
	PathPage* page = dynamic_cast<PathPage*>(wizard()->page(1));
	Utils::install(page->path());

	Utils::addRegistryMark();
	layout()->addWidget(new QLabel{ "Installation complete.\nClick \'Finish\' to exit.", this });
	
	_completed = true;
	emit completeChanged();
}

bool InstallPage::isComplete() const
{
	return _completed;
}