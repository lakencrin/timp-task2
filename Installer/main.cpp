#include "Wizard.h"
#include "Utils.h"

#include <QApplication>
#include <QDir>

#include <cstddef>

#include <Windows.h>


int main(int argc, char* argv[])
{
	QApplication app{ argc, argv };

	Wizard wizard;
	wizard.show();

	return app.exec();
}