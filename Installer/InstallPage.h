#pragma once
#include <qwizard.h>
class InstallPage : public QWizardPage
{
	bool _completed;

public:
	InstallPage();

	void initializePage() override;
	bool isComplete() const override;
};

