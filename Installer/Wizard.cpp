#include "Wizard.h"
#include "IntroPage.h"
#include "PathPage.h"
#include "InstallPage.h"

Wizard::Wizard(QWidget* parent)
	: QWizard{}
{
	int index = -1;
	index = addPage(new IntroPage);
	index = addPage(new PathPage);
	index = addPage(new InstallPage);
}