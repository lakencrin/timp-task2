#pragma once

#include <QString>
#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>

#include <Windows.h>

class Utils
{
public:
	static bool checkInstallation()
	{
		DWORD valueType, dataLength = 1000;
		BYTE data[1000];
		LSTATUS status = RegGetValueW(HKEY_CURRENT_USER, L"SOFTWARE\\task2\\Installed", L"Path", RRF_RT_ANY, &valueType, data, &dataLength);
		return status == ERROR_SUCCESS;
	}

	static void addRegistryMark()
	{
		const wchar_t keyPath[] = L"SOFTWARE\\task2";
		HKEY key;
		LSTATUS result = RegCreateKeyW(HKEY_CURRENT_USER, L"SOFTWARE\\task2\\Installed", &key);

		BYTE data[] = "Yes";
		result = RegSetValueExW(key, L"Path", 0, REG_SZ, data, sizeof(data));
	}

	static void removeRegistryMark()
	{
		RegDeleteTreeW(HKEY_CURRENT_USER, L"SOFTWARE\\task2");
	}

	static void install(const QString& installPath)
	{
		QDir dataDir = QDir{ QCoreApplication::instance()->applicationDirPath() };
		dataDir.cd("data");

		QString const path = dataDir.absolutePath();
		_install(installPath, path);
	}

private:
	static void _install(const QString& installPath, const QString& sourcePath)
	{
		QFileInfo fileInfo{ sourcePath };
		QString const name = fileInfo.absoluteFilePath();
		
		if (fileInfo.isDir())
		{
			QDir sourceDir{ sourcePath };
			QString dirPath = sourceDir.absolutePath();

			QDir installDir{ installPath };
			if (!installDir.exists())
			{
				installDir.mkpath(installDir.absolutePath());
			}

			for (auto& entryInfo : sourceDir.entryInfoList())
			{
				QString const entryName = entryInfo.fileName();
				if (entryName.isEmpty() || entryName == "." || entryName == "..")
					continue;

				QString const entryPath = entryInfo.absoluteFilePath();
				_install(installPath + '/' + entryName, entryPath);
			}
		}
		else
		{
			QFile{ sourcePath }.copy(installPath);
		}
	}
};