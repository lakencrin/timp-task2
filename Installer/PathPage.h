#pragma once
#include <QWizardPage>

class PathPage : public QWizardPage
{
	Q_OBJECT

	class QLabel* _pathLabel;
	class QPushButton* _openDialogButton;

public:
	PathPage();

	QString path() const;

private slots:
	void onOpenDialog();

private:
	void updatePath(const QString& path);
};

