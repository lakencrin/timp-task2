#pragma once
#include <QWizardPage>

class IntroPage : public QWizardPage
{
	bool const _isProgramInstalled;

public:
	IntroPage();

	int nextId() const override;
};

