#include "PathPage.h"

#include <QBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QFileDialog>
#include <QStandardPaths>

PathPage::PathPage()
	: QWizardPage{}
{
	QVBoxLayout* layout = new QVBoxLayout;
	setLayout(layout);
	{
		QHBoxLayout* pathLayout = new QHBoxLayout;
		QLabel* label = new QLabel{ "Select install directory:", this };
		pathLayout->addWidget(label);

		_openDialogButton = new QPushButton{ "...", this };
		QObject::connect(_openDialogButton, &QPushButton::clicked, this, &PathPage::onOpenDialog);

		_pathLabel = new QLabel{ this };

		pathLayout->addWidget(_openDialogButton);
		pathLayout->addWidget(_pathLabel);

		layout->addLayout(pathLayout);
	}
	updatePath(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
}

QString PathPage::path() const
{
	return _pathLabel->text();
}

void PathPage::onOpenDialog()
{
	QString const path = QFileDialog::getExistingDirectory(this, "", _pathLabel->text());
	updatePath(path);
}

void PathPage::updatePath(const QString& path)
{
	QDir installDir{ path };
	installDir.mkdir("task2");

	if (installDir.cd("task2"))
		_pathLabel->setText(installDir.absolutePath());
}