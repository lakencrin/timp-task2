#include "IntroPage.h"
#include "Utils.h"

#include <QLabel>
#include <QVBoxLayout>

IntroPage::IntroPage()
	: QWizardPage{}
	, _isProgramInstalled{ Utils::checkInstallation() }
{
	setTitle("Introduction");

	QLabel* label = new QLabel{ "asfawfwvfawfwfaw", this };
	label->setWordWrap(true);

	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget(label);
	setLayout(layout);

	if (_isProgramInstalled)
	{
		setFinalPage(true);

		QLabel* anotherLabel = new QLabel{ "Program is already installed. Click \'Close\' to exit the installator.", this };
		layout->addWidget(anotherLabel);
	}
}

int IntroPage::nextId() const
{
	return _isProgramInstalled ? -1 : QWizardPage::nextId();
}


