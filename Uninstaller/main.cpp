#include "Utils.h"

#include <Windows.h>

#include <iostream>
#include <fstream>
#include <string>

std::wstring dir = L"C:\\Users\\laken\\task2";

void recurseDelete(std::wstring filePath)
{
	if (filePath.back() != L'\\')
		filePath += '\\';

	std::wstring const filePathForSearch = filePath + L"*";

	WIN32_FIND_DATAW data;
	HANDLE searchHandle = FindFirstFileW(filePathForSearch.c_str(), &data);
	if (searchHandle != INVALID_HANDLE_VALUE)
	{
		do
		{
			std::wstring const fileName = data.cFileName;
			if (fileName == L"." || fileName == L"..")
				continue;

			std::wstring const childFilePath = filePath + data.cFileName;
			if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				recurseDelete(childFilePath);
			else
				DeleteFileW(childFilePath.c_str());
		} while (FindNextFileW(searchHandle, &data));
	}

	RemoveDirectoryW(filePath.c_str());
}

int main(int argc, char* argv[])
{ 
	Utils::removeRegistryMark();
	recurseDelete(Utils::getInstallationPath());
	Utils::selfdelete();

	system("pause");
	return 0;
}