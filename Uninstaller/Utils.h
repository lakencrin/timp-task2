#pragma once

#include <Windows.h>
#include <fstream>
#include <iostream>
#include <string>

class Utils
{
public:
	static void selfdelete()
	{
		DWORD result;

		wchar_t filename_raw[1000];
		result = GetModuleFileNameW(NULL, filename_raw, 1000);
		std::wstring filename{ filename_raw };

		int index = filename.rfind(L'\\');
		std::wstring exeDir = filename.substr(0, index + 1);

		std::wstring batCommand;
		batCommand += L":loc\n";
		batCommand += L"del \"" + std::wstring{ filename } + L"\"\n";
		batCommand += L"if exist \"" + std::wstring{ filename } + L"\" goto loc\n";
		batCommand += L"del %0\n";

		std::wstring batFilePath = exeDir + L"self-delete.bat";

		std::wfstream batFile;
		batFile.open(batFilePath, std::ios_base::out);
		if (!batFile.is_open())
		{
			std::cout << "Undefined error. Uninstall is not complete.\n";
		}

		batFile.write(batCommand.c_str(), batCommand.length());
		batFile.close();

		ShellExecuteW(0, nullptr, batFilePath.c_str(), nullptr, nullptr, 0);
	}
	static void removeRegistryMark()
	{
		RegDeleteTreeW(HKEY_CURRENT_USER, L"SOFTWARE\\task2");
	}

	static std::wstring getInstallationPath()
	{
		wchar_t filename_raw[1000];
		GetModuleFileNameW(NULL, filename_raw, 1000);
		
		std::wstring filename = filename_raw;

		int index = filename.rfind(L'\\');
		return filename.substr(0, index + 1);
	}
};